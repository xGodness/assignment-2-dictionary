%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%include "dict.inc"

%define buffer_size 255

section .rodata
input_msg: db "Enter the key: ", 0
buf_overflow_msg: db "Input length must not suprass 255 symbols", 10, 0
word_not_found_msg: db "Word by this key was not found", 10, 0

section .bss
buffer: resb buffer_size

section .text
global _start

_start:
	mov rdi, input_msg
	call print_string
	
	mov rdi, buffer
	mov rsi, buffer_size
	call read_word
	cmp rax, 0
	je .error_overflow
	
	mov rdi, buffer
	mov rsi, dict_head_ptr 
	call find_word
	cmp rax, 0
	je .not_found
	lea rdi, [rax+8]
	call string_length
	lea rdi, [rdi+rax+1]
	call print_string
	jmp exit

.error_overflow:
	mov rdi, buf_overflow_msg
	call string_length
	mov rdx, rax				; buf_size
	mov rax, 1					; sys_write
	mov rdi, 2					; stderr
	mov rsi, buf_overflow_msg	; *buf
	syscall
	call print_newline
	jmp exit

.not_found:
	mov rdi, word_not_found_msg
	call print_string
	jmp exit
	
