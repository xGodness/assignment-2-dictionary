%define dict_head_ptr 0

%macro colon 2
	%%next_node: dq dict_head_ptr		; save next node pointer
	db %1, 0							; save first argument as a key
	%2:									; define tag for the value
	%define dict_head_ptr %%next_node	; update head node pointer
%endmacro
