%include "lib.inc"

global find_word

section .text

; rdi -> pointer to the null-terminated string, key to search
; rsi -> pointer to the beginning of the dictionary
find_word:
	push r12
	push r13

	mov r12, rsi
	mov r13, rdi
	.find_word_loop:
		cmp r12, 0					; if pointer is null	
		je .find_word_fail			; then we reached end of the dictionary
		

		mov rdi, r13
		lea rsi, [r12+8] 
		call string_equals			; check key

		cmp rax, 0
		jne .find_word_success
		mov r12, [r12]

		jmp .find_word_loop			; if not found go to the next iteration
	.find_word_success:
		mov rax, r12
		jmp .find_word_ret
	.find_word_fail:
		xor rax, rax
		jmp .find_word_ret
	.find_word_ret:
		pop r13
		pop r12
		ret
